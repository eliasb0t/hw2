"use strict";

let setsFound = 0;
let remainingSeconds = 0;
let timerId = 'time';
let timer = null;

/**
 * Initiates everythinf
 */
function init() {
  document.getElementById('start-btn').addEventListener('click', toggleViews);
  document.getElementById('back-btn').addEventListener('click', toggleViews);
}

/**
 * Changes views between menu and game view
 */
function toggleViews() {
  // Hide start view & show game view
  let menuView = document.getElementById('menu-view');
  let gameView = document.getElementById('game-view');

  if (gameView.classList.contains('hidden')) {
    menuView.classList.add('hidden');
    gameView.classList.remove('hidden');
    startTimer();
    document.getElementById('set-count').innerHTML = '0';
    document.getElementById('refresh-btn').disabled = '';
    setsFound = 0;
    let isEasy = document.getElementById('menu-view').children[0].children[3].children[0].children[0].checked;
    clearBoard();
    generateBoard(isEasy);

    document.getElementById('refresh-btn').addEventListener('click', refreshBoard);
  } else {
    menuView.classList.remove('hidden');
    gameView.classList.add('hidden');
    clearInterval(timer);
  }
}

/**
 * Generates the board
 * @param {boolean} isEasy - if the game is in easy mode
 */
function generateBoard(isEasy) {

  let board = document.getElementById('board');

  let cards = 0;

  if (isEasy) {
    cards = 9;
  } else {
    cards = 12;
  }

  for (let i = 0; i < cards; i++) {
    let card = generateUniqueCard(isEasy);
    board.appendChild(card);
  }

}

/**
 * Marks cards as selected
 * then checks if there are 3
 * then checks if they're a set
 * @param {HTMLElement} card - the card clicked
 */
function cardSelected(card) {
  let selectedCard = "";

  if (card.target.id === "") {
    selectedCard = card.target.parentNode;
  } else {
    selectedCard = card.target;
  }

  selectedCard.classList.add('selected');

  let selectedCards = document.getElementsByClassName("selected");

  if (selectedCards.length === 3) {
    if (isASet(selectedCards)) {
      setsFound++;
      document.getElementById('set-count').innerHTML = setsFound;
      setFound(selectedCards);

    } else {
      notASet(selectedCards);
    }
  }
}

/**
 * Runs if cards selected is a set
 * @param {array} sets - cards selected
 */
function setFound(sets) {
  for (let i = 0; i < sets.length; i++) {
    let card = sets[i];
    sets[i].classList.add('hide-imgs');
    let message = document.createElement("p");
    message.innerHTML = 'SET!';
    card.appendChild(message);
  }

  setTimeout(function() {
    replaceCards(sets);
  }, 1000);
}

/**
 * Runs if selected cards are not a set
 * @param {array} cards - array of cards selected
 */
function notASet(cards) {
  let ids = [];

  for (let i = 0; i < cards.length; i++) {
    ids.push(cards[i].id);
  }

  for (let i = 0; i < ids.length; i++) {
    let card = document.getElementById(ids[i]);
    card.classList.add('hide-imgs');
    let message = document.createElement("p");
    message.innerHTML = 'Not a Set!';
    card.appendChild(message);
    setTimeout(function() {
      card.classList.remove('hide-imgs');
      card.classList.remove('selected');
      message.remove();
    }, 1000);
  }
}

/**
 * Clears the board
 */
function clearBoard() {
  let ids = [];
  let cards = document.getElementsByClassName('card');

  for (let i = 0; i < cards.length; i++) {
    ids.push(cards[i].id);
  }

  for (let i = 0; i < ids.length; i++) {
    let card = document.getElementById(ids[i]);
    card.remove();
  }
}

/**
 * Generates a unique card
 * @param {array} cards - a list of cards
 */
function replaceCards(cards) {
  let ids = [];

  for (let i = 0; i < cards.length; i++) {
    ids.push(cards[i].id);
  }

  for (let i = 0; i < ids.length; i++) {
    let isEasy = document.getElementById('menu-view').children[0].children[3].children[0].children[0].checked;
    let card = document.getElementById(ids[i]);
    let newCard = generateUniqueCard(isEasy);
    card.parentNode.replaceChild(newCard, card);
  }
}

/**
 * Refreshes the board
 */
function refreshBoard() {
  let cards = document.querySelectorAll('.card');
  replaceCards(cards);
}

/**
 * Generates a unique card
 * @param {boolean} isEasy - if the game is in easy mode
 * @return {HTMLElement}  - the card generated
 */
function generateUniqueCard(isEasy) {
  let cardAttributes = generateRandomAttributes(isEasy);

  let attributes = cardAttributes[0] + '-' + cardAttributes[1] + '-' + cardAttributes[2];

  let id = attributes + '-' + cardAttributes[3];

  while (document.getElementById(id) !== null) {
    cardAttributes = generateRandomAttributes(isEasy);
    attributes = cardAttributes[0] + '-' + cardAttributes[1] + '-' + cardAttributes[2];
    id = attributes + '-' + cardAttributes[3];
  }

  let card = document.createElement("div");
  card.id = attributes + '-' + cardAttributes[3];
  card.classList.add('card');
  card.addEventListener('click', cardSelected);

  for (let i = 0; i < cardAttributes[3]; i++) {
    let newImg = document.createElement('img');
    newImg.src = 'img/' + attributes + '.png';
    newImg.alt = id;
    card.appendChild(newImg);
  }

  return card;
}

/**
 * Generates an array of random attributes
 * @param {boolean} isEasy - if the game is in easy mode
 * @return {array} - an array of attributes
 */
function generateRandomAttributes(isEasy) {
  let STYLES = ['solid', 'outline', 'striped'];
  let COLORS = ['green', 'purple', 'red'];
  let SHAPES = ['diamond', 'oval', 'squiggle'];
  let COUNTS = [1, 2, 3];

  let STYLE = '';

  if (isEasy === true) {
    STYLE = STYLES[0];
  } else {
    STYLE = STYLES[Math.floor(Math.random() * 3)];
  }

  let COLOR = COLORS[Math.floor(Math.random() * 3)];
  let SHAPE = SHAPES[Math.floor(Math.random() * 3)];
  let COUNT = COUNTS[Math.floor(Math.random() * 3)];

  return [STYLE, SHAPE, COLOR, COUNT];
}

/**
 * Starts the timer interval
 * after getting the time selected
 */
function startTimer() {
  let select = document.getElementsByTagName('select')[0];
  remainingSeconds = parseInt(select.options[select.selectedIndex].value);

  advanceTimer(remainingSeconds);

  timer = setInterval(function() {
    if (remainingSeconds <= 1) {
      clearInterval(timer);
      endGame();
    }
    remainingSeconds--;
    advanceTimer(remainingSeconds);
  }, 1000);
}

/**
 * Ends the game.
 */
function endGame() {
  let selected = document.getElementsByClassName('card');
  let ids = [];

  for (let i = 0; i < selected.length; i++) {
    ids.push(selected[i].id);
  }

  for (let i = 0; i < ids.length; i++) {
    let card = document.getElementById(ids[i]);
    card.classList = 'card';
    card.removeEventListener('click', cardSelected);
  }

  document.getElementById('refresh-btn').disabled = true;
}

/**
 * Advances the timer shown by 1 second
 * @param {int} t - the number of seconds remaining
 */
function advanceTimer(t) {
  let time = document.getElementById(timerId);
  let minutes = Math.floor(t / 60);
  let seconds = t - minutes * 60;
  minutes = ("0" + minutes).slice(-2);
  seconds = ("0" + seconds).slice(-2);
  time.innerHTML = minutes + ":" + seconds;
}

/**
 * Checks to see if the three selected cards make up a valid set. This is done by comparing each
 * of the type of attribute against the other two cards. If each four attributes for each card are
 * either all the same or all different, then the cards make a set. If not, they do not make a set
 * @param {DOMList} selected - list of all selected cards to check if a set.
 * @return {boolean} true if valid set false otherwise.
 */
function isASet(selected) {
  let attribute = [];
  for (let i = 0; i < selected.length; i++) {
    attribute.push(selected[i].id.split("-"));
  }
  for (let i = 0; i < attribute[0].length; i++) {
    let diff = attribute[0][i] !== attribute[1][i] &&
              attribute[1][i] !== attribute[2][i] &&
              attribute[0][i] !== attribute[2][i];
    let same = attribute[0][i] === attribute[1][i] &&
                  attribute[1][i] === attribute[2][i];
    if (!(same || diff)) {
      return false;
    }
  }
  return true;
}

window.addEventListener('load', init);